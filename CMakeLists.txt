cmake_minimum_required(VERSION 3.12)
project(SokeT)

set(CMAKE_CXX_STANDARD 14)

add_executable(SokeT main.cpp)